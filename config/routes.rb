Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'pages#home'

  get 'rooms', to: 'pages#rooms'
  get 'activity', to: 'pages#activity'
  get 'dining', to: 'pages#dining'
  get 'contact', to: 'pages#contact'

  #get 'admin', to: 'pages#admin' #subject to change

  # Admin Management
  namespace :admin do
    root 'pages#home'
    resources :bookings, :payment_methods, :price_categories, :room_types, :rooms, :seasonal_rates, :view_types
    get 'daily_report', to: 'pages#daily_report'
    get 'price_selection', to: 'pages#price_selection'
  end
  
end
