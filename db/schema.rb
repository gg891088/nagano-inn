# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_08_210324) do

  create_table "bills", force: :cascade do |t|
    t.string "transaction_number", null: false
    t.integer "number_of_rooms", null: false
    t.integer "total_price", null: false
    t.datetime "billing_date", null: false
    t.boolean "is_paid", default: false
    t.datetime "payment_date"
    t.integer "guest_id"
    t.integer "payment_method_id"
    t.index ["guest_id"], name: "index_bills_on_guest_id"
    t.index ["payment_method_id"], name: "index_bills_on_payment_method_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.integer "room_paid_price", null: false
    t.date "from_date", null: false
    t.date "until_date", null: false
    t.integer "bill_id"
    t.integer "room_id"
    t.index ["bill_id"], name: "index_bookings_on_bill_id"
    t.index ["room_id"], name: "index_bookings_on_room_id"
  end

  create_table "guests", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "email", null: false
  end

  create_table "neighborhoods", force: :cascade do |t|
    t.integer "left_door_no"
    t.integer "front_door_no"
    t.integer "right_door_no"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "payment_method_name", null: false
  end

  create_table "price_categories", force: :cascade do |t|
    t.string "category_name", null: false
    t.decimal "base_price", null: false
    t.integer "week_day_rate", default: 0, null: false
    t.integer "week_end_rate", default: 0, null: false
    t.integer "season_id"
    t.integer "room_type_id"
    t.integer "view_type_id"
    t.index ["room_type_id"], name: "index_price_categories_on_room_type_id"
    t.index ["season_id"], name: "index_price_categories_on_season_id"
    t.index ["view_type_id"], name: "index_price_categories_on_view_type_id"
  end

  create_table "room_types", force: :cascade do |t|
    t.string "room_type_name", null: false
    t.decimal "room_type_rate", default: "0.0", null: false
    t.integer "number_of_places", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.integer "door_no", null: false
    t.integer "floor_no", null: false
    t.boolean "is_clean", default: false
    t.boolean "is_available", default: false
    t.integer "price_category_id"
    t.integer "neighborhood_id"
    t.index ["neighborhood_id"], name: "index_rooms_on_neighborhood_id"
    t.index ["price_category_id"], name: "index_rooms_on_price_category_id"
  end

  create_table "seasons", force: :cascade do |t|
    t.string "season_name", null: false
    t.decimal "season_rate", default: "0.0", null: false
    t.date "from_date", null: false
    t.date "until_date", null: false
  end

  create_table "view_types", force: :cascade do |t|
    t.string "view_type_name", null: false
    t.decimal "view_type_rate", default: "0.0", null: false
  end

  add_foreign_key "bills", "guests"
  add_foreign_key "bills", "payment_methods"
  add_foreign_key "bookings", "bills"
  add_foreign_key "bookings", "rooms"
  add_foreign_key "price_categories", "room_types"
  add_foreign_key "price_categories", "seasons"
  add_foreign_key "price_categories", "view_types"
  add_foreign_key "rooms", "neighborhoods"
  add_foreign_key "rooms", "price_categories"
end
