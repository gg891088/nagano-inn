class AddBookingRelatedTables < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.integer :room_paid_price, null: false
      t.date :from_date, null: false
      t.date :until_date, null: false
    end
    create_table :bills do |t|
      t.string :transaction_number, null: false
      t.integer :number_of_rooms, null: false
      t.integer :total_price, null: false
      t.datetime :billing_date, null: false
      t.boolean :is_paid, default: false
      t.datetime :payment_date
      
    end
    create_table :guests do |t|
      t.string :first_name , null: false
      t.string :last_name , null: false
      t.string :email, null: false
    end
    create_table :payment_methods do |t|
      t.string :payment_method_name, null: false
    end
  end
end
