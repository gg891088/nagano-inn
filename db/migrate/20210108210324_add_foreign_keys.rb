class AddForeignKeys < ActiveRecord::Migration[6.0]
  def change
    # rooms table
    add_reference :rooms, :price_category, foreign_key: true
    add_reference :rooms, :neighborhood, foreign_key: true

    # price_categories table
    add_reference :price_categories, :season, foreign_key: true
    add_reference :price_categories, :room_type, foreign_key: true
    add_reference :price_categories, :view_type, foreign_key: true

    # bookings table
    add_reference :bookings, :bill, foreign_key: true
    add_reference :bookings, :room, foreign_key: true

    # bills table
    add_reference :bills, :guest, foreign_key: true
    add_reference :bills, :payment_method, foreign_key: true

  end
end
