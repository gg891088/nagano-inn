class AddRoomRelatedTables < ActiveRecord::Migration[6.0]
  def change
    create_table :rooms do |t|
      t.integer :door_no, null: false
      t.integer :floor_no, null: false
      t.boolean :is_clean, default: false
      t.boolean :is_available, default: false
    end
    create_table :neighborhoods do |t|
      t.integer :left_door_no
      t.integer :front_door_no
      t.integer :right_door_no
    end
  end
end
