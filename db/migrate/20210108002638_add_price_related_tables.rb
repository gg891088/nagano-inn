class AddPriceRelatedTables < ActiveRecord::Migration[6.0]
  def change
    create_table :price_categories do |t|
      t.string :category_name, null: false
      t.decimal :base_price, null: false
      t.integer :week_day_rate, null: false, default: 0
      t.integer :week_end_rate, null: false, default: 0
    end
    create_table :seasons do |t|
      t.string :season_name, null: false
      t.decimal :season_rate, null: false, default: 0
      t.date :from_date, null: false
      t.date :until_date, null: false
    end
    create_table :room_types do |t|
      t.string :room_type_name, null: false
      t.decimal :room_type_rate, null: false, default: 0
      t.integer :number_of_places, null: false
    end
    create_table :view_types do |t|
      t.string :view_type_name, null: false
      t.decimal :view_type_rate, null: false, default: 0
    end
  end
end