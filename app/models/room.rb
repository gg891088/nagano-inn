class Room < ApplicationRecord
    belongs_to :price_category
    belongs_to :neighborhood

    has_many :bookings
    has_many :bills, through: :bookings
end