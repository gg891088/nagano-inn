class PriceCategory < ApplicationRecord
    belongs_to :season
    belongs_to :room_type
    belongs_to :view_type
    has_many :rooms
end