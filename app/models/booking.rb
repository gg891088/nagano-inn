class Booking < ApplicationRecord
    # Should have a composite primary key here but it's more complex to do so with rails
    # I'll simply leave the table with the standard id as the primary key and add bill_id 
    # and room_id as foreign keys using has_many :through association
    belongs_to :bill
    belongs_to :room
    
end